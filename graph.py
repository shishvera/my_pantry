"""
Аргументы командной строки: (default 0 100 3)

1. 0 - генерировать рандомную матрицу
   1 - считать из файла

2. Размер генерируемой матрицы или имя файла

3. 1 - сохранить круговую картинку
   2 - сохранить гравитационную каритинку
   3 - показать интерактивное окно
   4 - сохранить несколько гравитационных картинок с разным количеством ребер

4. Путь к матрице P

5. k - количество топ-ОКВЭДов.

6. Путь к списку окведов matching.csv
"""

import numpy as np
import pandas as pd
import sys
import os
import os.path

import graph_tool as gt
from graph_tool.all import arf_layout, random_layout, GraphWindow, GraphWidget
from gi.repository import Gtk, Gdk, GdkPixbuf, GObject, GLib


def tops(path_p, path_matching, k=2):
    """
    :param path_p: string, путь к матрице P
    :param k: int, количество топ-ОКВЭДоа для каждого ОКВЭДа
    :param path_matching: string, путь к файлу с соответсвиями
    :return: np.array, shape=(num_okveds, k * k)
    """
    data_p = np.genfromtxt(path_p, delimiter=' ')
    p = pd.DataFrame(data=data_p,
                     index=range(data_p.shape[0]),
                     columns=range(data_p.shape[1]))
    okv = pd.read_csv(path_matching, sep=';')

    # выбираем топ фирм по окведам
    order = np.argsort(-p.values, axis=0)[:k, :]
    top_firm = pd.DataFrame(p.index[order],
                            index=['top{}'.format(i) for i in range(1, k + 1)],
                            columns=p.columns)
    # выбираем окведы для фирм
    data_top = []
    for okved in top_firm.columns:
        lst = []
        for f in list(top_firm[okved]):
            if f in list(okv['id']):
                lst.append(float(list(okv[okv['id'] == f]['okved'])[0]))
            else:
                lst.append('fail')
        data_top.append(lst)

    # это будем использовать вместо цикла (будет быстрее),
    # когда в файле не будет фейлов(повторяющихся строк и недостающих id)
    # data_top = [list(okv[okv['id'].isin(list(top_firm[okved]))]['okved']) for okved in top_firm.columns]
    top = pd.DataFrame(data=data_top,
                       columns=['top{}'.format(i) for i in range(1, k + 1)],
                       index=p.columns)
    return top.as_matrix()


def init_lambda(size=100):
    """
    Возвращает рандомную верхнетреугольную квадратную матрицу размера (size, size), со значениями из [0, 1].
    :param size: int, height of square matrix
    :return: np.array, shape=(size, size), Lambda matrix
    """
    lamb = np.triu(np.random.rand(size, size), 1)
    lamb[lamb < 0.7] = 0
    return lamb


def read_lambda(path):
    """
    Считывает квадратную матрицу из файла, который находится в path.
    Возвращает эту матрицу, округляя все ее элемента меньшие 10^(-5) до 0.
    :param path: string, path to matrix to read
    :return: np.array, Lambda matrix
    """
    lamb = np.genfromtxt(path, dtype='str').astype(float)
    lamb[lamb < 1e-5] = 0
    return lamb


def init_graph(lamb, path_p=None, path_matching=None, num_top=None):
    """
    Функция создает взвешенный направленный граф на основе матрицы lamb.
    Возвращает граф с его свойствами.
    :param lamb: np.array, Lambda matrix
    :param path_p: string, путь к матрице P
    :param path_matching: string, путь к файлу с соответсвиями
    :return: g - объект (граф) библиотеки graph-tool
             edge_weight, new_edge_property('float'), properties map в библиотеке graph tool (вес ребер графа)
             show_edge, new_edge_property('bool'), properties map в библиотеке graph tool (показывать ли ребро в графе)
             deg, int, полная (входящая и исходящая) степень ребер
             top_okv, properties map вершин в библиотеке graph tool (топ ближайших соседей)

    """
    size = lamb.shape[0]

    g = gt.Graph()
    g.add_vertex(len(lamb))
    g.add_edge_list(np.transpose(lamb.nonzero()))

    w = []
    edge_weight = g.new_edge_property('float')
    for i in range(size):
        for j in range(size):
            if lamb[i, j] != 0:
                w.append(lamb[i, j])

    i = 0
    for e in g.edges():
        edge_weight[e] = w[i]
        i += 1

    show_edge = g.new_edge_property('bool')
    for v in g.edges():
        show_edge[v] = False

    deg = g.new_vertex_property('int')
    for v in g.vertices():
        deg[v] = 1

    if path_p is not None:
        top_okv = g.new_vertex_property('string')
        i = 0
        top = tops(path_p, path_matching, num_top)
        for v in g.vertices():
            top_okv[v] = ', '.join(str(e) for e in list(top[i, :]))
            i += 1
        return g, edge_weight, show_edge, deg, top_okv
    else:
        return g, edge_weight, show_edge, deg


def save_graph_circle(gr, path='graph_krug.pdf'):
    """
    Функция для отрисовки графа в виде круга и сохранения картинки на диск в path.
    :param gr: объект (граф) библиотеки graph-tool
    :param path: string, path to save picture
    """
    deg1 = gr.degree_property_map('total')
    state = gt.inference.minimize_nested_blockmodel_dl(gr, B_max=3, deg_corr=True)
    gt.draw.draw_hierarchy(state, vertex_size=deg1, output=path)


def save_graph_arf(gr, edge_weight, path='graph_grav.pdf'):
    """
    Функция для отрисовки графа в виде Force-directed graph и сохранения картинки на диск в path.
    :param gr: объект (граф) библиотеки graph-tool
    :param edge_weight: new_edge_property('float'), properties map в библиотеке graph tool (вес ребер графа)
    :param path: string, path to save picture
    """
    deg.a = 2 * gr.degree_property_map('total').a
    state = gt.inference.minimize_blockmodel_dl(gr, B_max=3)
    pos = gt.draw.arf_layout(gr, max_iter=0)

    gt.draw.graph_draw(gr, pos=pos,
                       geometry=(5000, 5000),
                       vertex_size=deg,
                       # vertex_pen_width=0.5,
                       # vertex_text_position=45,
                       vertex_halo_size=0,
                       vertex_text=g.vertex_index,
                       vertex_text_color='k',
                       edge_pen_width=gt.draw.prop_to_size(edge_weight, mi=0, ma=3, power=5),
                       vertex_fill_color=state.get_blocks(),
                       edge_marker_size=10,
                       output=path)


def save_many_graph_arf(gr, edge_weight, show_edge, deg, lamb, s=5, max_count=50, path_dir='./frames'):
    """
    Функция для сохранения картинок графа с различным количеством ребер.
    Картинки сохраняются в директорию path_dir. На каждом этапе добавляется s ребер.
    :param path_dir: string, path to folder where to store pictures
    :param gr: объект (граф) из библиотеки graph-tool
    :param edge_weight: new_edge_property('float'), properties map в библиотеке graph tool (вес ребер графа)
    :param show_edge: new_edge_property('bool'), properties map в библиотеке graph tool (показывать ли ребро в графе)
    :param deg: int, степени вершин графа
    :param lamb: np.array, square Lambda matrix
    :param s: int, number of edges to add to each graph picture
    :param max_count: int, maximum number of graph pictures to save
    """
    # ToDo: we should to rewrite this global thing and nested functions
    global count
    count = 0

    w = list(np.reshape(lamb, lamb.shape[0] ** 2, order='C'))
    w = filter(lambda a: a != 0, w)
    w.sort()

    mask = filter(lambda a: a % s == 0, range(len(w)))
    limits = [w[i] for i in mask]

    ed = list(gr.edges())

    state = gt.inference.minimize_blockmodel_dl(gr, B_max=3)
    pos = random_layout(gr)

    if not os.path.exists(path_dir):
        os.mkdir(path_dir)

    win = Gtk.OffscreenWindow()
    win.set_default_size(1000, 1000)
    win.graph = GraphWidget(gr, pos,
                            vertex_size=deg,
                            vertex_pen_width=0.5,
                            vertex_halo_size=0,
                            vertex_text=g.vertex_index,
                            vertex_text_color='k',
                            edge_pen_width=gt.draw.prop_to_size(edge_weight, mi=0, ma=3, power=5),
                            vertex_fill_color=state.get_blocks(),
                            edge_marker_size=10)
    win.add(win.graph)

    def update_state():

        arf_layout(gr, pos=pos, max_iter=3)
        if len(limits) == 0:
            sys.exit(0)
        for e in ed:
            if edge_weight[e] > limits[len(limits) - 1]:
                show_edge[e] = True

        gr.set_edge_filter(show_edge)
        deg.a = gr.degree_property_map('total').a * 2
        limits.pop(len(limits) - 1)

        win.graph.regenerate_surface()
        win.graph.queue_draw()

        global count
        pixbuf = win.get_pixbuf()
        pixbuf.savev(r'{0}/sirs{1}.jpeg'.format(path_dir, count), 'jpeg', ['quality'], ['100'])
        if count > max_count:
            sys.exit(0)
        count += 1
        return True

    GLib.idle_add(update_state)
    win.connect('delete_event', Gtk.main_quit)

    win.show_all()
    Gtk.main()
    return None


def show_window(gr, edge_weight, show_edge, deg, lamb, top_okv, s=5):
    """
    В интерактивное окно выводятся последовательно гравитационные картинки с добавлением каждый раз s ребер.
    :param top_okv: properties map вершин в библиотеке graph tool (топ ближайших соседей)
    :param gr: объект (граф) библиотеки graph-tool
    :param edge_weight: new_edge_property('float'), properties map в библиотеке graph tool (вес ребер графа)
    :param show_edge: new_edge_property('bool'), properties map в библиотеке graph tool (показывать ли ребро в графе)
    :param deg: int, степени вершин графа
    :param lamb: np.array, square matrix
    :param s: int, number of edges to add to each graph picture
    """
    w = list(np.reshape(lamb, lamb.shape[0] ** 2, order='C'))
    w = filter(lambda a: a != 0, w)
    w.sort()

    mask = filter(lambda a: a % s == 0, range(len(w)))
    limits = [w[i] for i in mask]

    ed = list(gr.edges())

    state = gt.inference.minimize_blockmodel_dl(gr, B_max=3)
    pos = random_layout(gr)

    win = GraphWindow(gr, pos,
                      geometry=(1000, 1000),
                      vertex_size=deg,
                      vertex_pen_width=0.5,
                      vertex_halo_size=0,
                      display_props=top_okv,
                      vertex_text=g.vertex_index,
                      vertex_text_color='k',
                      edge_pen_width=gt.draw.prop_to_size(edge_weight, mi=0.5, ma=5, power=5),
                      vertex_fill_color=state.get_blocks(),
                      edge_marker_size=10)

    def update_state():
        if len(limits) == 0:
            sys.exit(0)
        arf_layout(gr, pos=pos, max_iter=3)
        for e in ed:
            if edge_weight[e] > limits[len(limits) - 1]:
                show_edge[e] = True

        gr.set_edge_filter(show_edge)
        deg.a = gr.degree_property_map('total').a
        limits.pop(len(limits) - 1)

        win.graph.regenerate_surface()
        win.graph.queue_draw()

        return True

    GLib.idle_add(update_state)
    win.connect('delete_event', Gtk.main_quit)

    win.show_all()
    Gtk.main()


if __name__ == '__main__':
    if len(sys.argv) > 1:
        if int(sys.argv[1]) == 0:
            n = int(sys.argv[2])
            matrix = init_lambda(n)
        else:
            name = sys.argv[2]
            matrix = read_lambda(name)

        out_type = int(sys.argv[3])
        if out_type == 1:
            g, edge_weights, show, deg = init_graph(matrix)
            save_graph_circle(g)
        if out_type == 2:
            g, edge_weights, show, deg = init_graph(matrix)
            save_graph_arf(g, edge_weights)
        if out_type == 3:
            path_p = sys.argv[4]
            path_okv = sys.argv[6]
            num_top = int(sys.argv[5])
            g, edge_weights, show, deg, top_okv = init_graph(matrix, path_p, path_okv, num_top)
            show_window(g, edge_weights, show, deg, matrix, top_okv, 5)
        if out_type == 4:
            g, edge_weights, show, deg = init_graph(matrix)
            save_many_graph_arf(g, edge_weights, show, deg, matrix, 5, 200)
    else:
        matrix = init_lambda()
        g, edge_weights, show, deg = init_graph(matrix)
        show_window(g, edge_weights, show, deg, matrix, top_okv=None, s=5)
